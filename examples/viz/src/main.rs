mod drawer;
mod gui;

use forceatlas2::*;
use parking_lot::RwLock;
use std::{io::BufRead, sync::Arc, thread, time::Duration};

const STANDBY_SLEEP: Duration = Duration::from_millis(50);
const COMPUTE_SLEEP: Duration = Duration::from_millis(1);
type T = f32;

fn main() {
	let file = std::fs::File::open(std::env::args().nth(1).expect("Usage: viz <csv_file>"))
		.expect("Cannot open file");

	let mut nodes = Vec::new();
	let mut edges = Vec::<((usize, usize), T)>::new();
	for (i, line) in std::io::BufReader::new(file).lines().enumerate() {
		let line = line.expect("Error reading CSV");
		let mut columns = line.split(&[' ', '\t', ',', ';'][..]);
		if let (Some(n1), Some(n2)) = (columns.next(), columns.next()) {
			if let (Ok(n1), Ok(n2)) = (n1.parse::<usize>(), n2.parse::<usize>()) {
				if n1 >= nodes.len() || n2 >= nodes.len() {
					nodes.extend((nodes.len()..=n1.max(n2)).map(|_| AbstractNode::default()));
				}
				if n1 != n2 {
					edges.push((
						if n1 < n2 { (n1, n2) } else { (n2, n1) },
						columns.next().map_or(1.0, |w| {
							w.parse().unwrap_or_else(|_| {
								eprintln!("Ignored weight line {} has bad number format", i);
								1.0
							})
						}),
					));
					nodes[n1].mass += 1.0;
					nodes[n2].mass += 1.0;
				} else {
					eprintln!("Ignored line {} has loop", i);
				}
			} else {
				eprintln!("Ignored line {} has bad number format", i);
			}
		} else {
			eprintln!("Ignored line {} has <2 columns", i);
		}
	}

	println!("Nodes: {}", nodes.len());

	let settings = Settings {
		theta: 0.5,
		ka: 1.0,
		kg: 1.0,
		kr: 1.0,
		lin_log: false,
		prevent_overlapping: Some(100.),
		speed: 0.01,
		strong_gravity: false,
	};

	let mut rng = rand::thread_rng();
	let layout = Arc::new(RwLock::new((
		false,
		Layout::<T, 2>::from_abstract(
			settings.clone(),
			nodes.iter().cloned().enumerate(),
			edges.clone(),
			&mut rng,
		),
		Layout::<T, 3>::from_abstract(
			settings.clone(),
			nodes.into_iter().enumerate(),
			edges,
			&mut rng,
		),
	)));

	let compute = Arc::new(RwLock::new(false));
	let settings = Arc::new(RwLock::new(settings));
	let nb_iters = Arc::new(RwLock::new(0usize));

	thread::spawn({
		let compute = compute.clone();
		let layout = layout.clone();
		let nb_iters = nb_iters.clone();
		move || loop {
			thread::sleep(if *compute.read() {
				let mut nb_iters = nb_iters.write();
				let mut layout_guard = layout.write();
				if layout_guard.0 {
					let (_sw, _tr, _max_tr) = layout_guard.2.iteration();
					//println!("Global speed: {}", layout_guard.2.global_speed());
				} else {
					let (_sw, _tr, _max_tr) = layout_guard.1.iteration();
					//println!("Global speed: {}", layout_guard.1.global_speed());
				}
				*nb_iters += 1;
				COMPUTE_SLEEP
			} else {
				STANDBY_SLEEP
			});
		}
	});

	gui::run(compute, layout, settings, nb_iters);
}
