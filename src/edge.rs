/// Collection of edges for the layout
pub trait Edges<T, Id> {
	/// Create an iterator through the edges `((node_1, node_2), weight)`
	fn iter_edges(&self) -> impl Iterator<Item = ((Id, Id), T)>;
}

/// Collection of edges for the layout, stored in a Vec
pub type EdgeVec<T, Id> = Vec<((Id, Id), T)>;

impl<T: Clone, Id: Clone> Edges<T, Id> for EdgeVec<T, Id> {
	fn iter_edges(&self) -> impl Iterator<Item = ((Id, Id), T)> {
		self.iter().cloned()
	}
}

/// Collection of edges for the layout, stored in a BTreeMap
pub type EdgeBTreeMap<T, Id> = std::collections::BTreeMap<(Id, Id), T>;

impl<T: Clone, Id: Clone> Edges<T, Id> for EdgeBTreeMap<T, Id> {
	fn iter_edges(&self) -> impl Iterator<Item = ((Id, Id), T)> {
		self.iter()
			.map(|(edge, weight)| (edge.clone(), weight.clone()))
	}
}

/// Collection of edges for the layout, stored in a HashMap
pub type EdgeHashMap<T, Id> = std::collections::HashMap<(Id, Id), T>;

impl<T: Clone, Id: Clone> Edges<T, Id> for EdgeHashMap<T, Id> {
	fn iter_edges(&self) -> impl Iterator<Item = ((Id, Id), T)> {
		self.iter()
			.map(|(edge, weight)| (edge.clone(), weight.clone()))
	}
}
