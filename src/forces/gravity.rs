use crate::{layout::*, node::*, util::*};

use rayon::prelude::*;

pub(crate) fn apply_gravity<
	T: Coord + Send + Sync,
	const N: usize,
	E,
	O: Nodes<T, N, Id>,
	Id: Sync,
>(
	layout: &mut Layout<T, N, E, O, Id>,
) {
	layout.nodes.par_iter_nodes_mut().for_each(|node| {
		let mut d = T::zero();
		for x in node.pos {
			d += x * x;
		}
		if d.is_zero() {
			return;
		}
		node.speed -= node.pos * (node.mass + T::one()) * layout.settings.kg / d.sqrt();
	})
}

pub(crate) fn apply_gravity_sg<
	T: Coord + Send + Sync,
	const N: usize,
	E,
	O: Nodes<T, N, Id>,
	Id: Sync,
>(
	layout: &mut Layout<T, N, E, O, Id>,
) {
	layout.nodes.par_iter_nodes_mut().for_each(|node| {
		node.speed -= node.pos * (node.mass + T::one()) * layout.settings.kg;
	})
}
